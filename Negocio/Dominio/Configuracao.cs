﻿namespace microondas.Negocio.Dominio
{
    using System;

    public class Configuracao
    {
        private int? _Potencia;
        public int? Potencia
        {
            get
            {
                return this._Potencia != null ? this._Potencia : 10;
            }
            set
            {
                this._Potencia = value != null ? value : new Nullable<int>();
            }
        }
        public DateTime Tempo { get; set; }

        public char _Indicador { get; set; }

        //public void ConfigurarInicioRapido()
        //{
        //    this.Potencia = 8;
        //    this.Tempo = new DateTime(0001, 1, 1, 0, 0, 30);
        //}

        public Configuracao()
        {
            this.Tempo = new DateTime();
            this._Indicador = '.';
        }
    }
}