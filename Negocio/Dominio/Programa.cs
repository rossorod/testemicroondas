﻿namespace microondas.Negocio.Dominio
{
    using System;
    using System.Collections.Generic;

    public class Programa : Configuracao
    {
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public new int Tempo { get; set; }

        public Programa(string nome, string descricao, int? potencia, int tempo, char indicador)
        {
            if (!string.IsNullOrEmpty(nome) || !string.IsNullOrEmpty(descricao) || !string.IsNullOrEmpty(indicador.ToString()))
            {
                this.Nome = nome;
                this.Descricao = descricao;
                this.Potencia = potencia;
                this._Indicador = indicador;
                this.Tempo = tempo;
            }
            else
            {
                throw new Exception("Alguns parametros não estão preenchidos");
            }
        }

        public Programa()
        {

        }

        public static List<Programa> IniciarProgramas()
        {
            List<Programa> programas = new List<Programa>();

            var programa = new Programa("Frango", "Descongelar Frango", 10, 120, 'F');
            programas.Add(programa);

            programa = new Programa("Arroz", "Fazer arroz ", 3, 90, 'A');
            programas.Add(programa);

            programa = new Programa("Pipoca", "Preparar Pipoca", 5, 45, 'P');
            programas.Add(programa);

            programa = new Programa("Pizza", "Assar Pizzas ", 7, 90, 'Z');
            programas.Add(programa);

            programa = new Programa("Macarrao", "Cozinhar Macarrão", 7, 90, 'M');
            programas.Add(programa);

            return programas;
        }
        static void Main()
        {
        }
    }
}