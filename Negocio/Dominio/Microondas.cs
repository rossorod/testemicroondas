﻿namespace microondas.Negocio.Dominio
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class Microondas : IMicroondas
    {
        private void ValidarPotencia(int? potencia)
        {
            if (potencia < 1 || potencia > 10)
            {
                throw new Exception("A potência está fora dos parametros (1-10)");
            }
        }

        private void ValidarTempo(DateTime tempo)
        {
            var tempoEmSegundos = tempo.Minute * 60 + tempo.Second;
            if (tempoEmSegundos > 120)
            {
                throw new Exception("O tempo não pode ser maior que 2 minutos");
            }
            if (tempoEmSegundos < 1)
            {
                throw new Exception("É preciso definir o tempo");
            }
        }

        private Programa validarPrograma(string nome, List<Programa> programas)
        {
            var listaDeProgramas = programas;
            var programa = listaDeProgramas.Where(w => w.Nome.Equals(nome)).FirstOrDefault();
            if (programa == null)
            {
                throw new Exception("Não há programa compatível com a comida informada");
            }
            return programa;
        }

        public void Executar(Configuracao Configuracao)
        {
            try
            {
                this.ValidarTempo(Configuracao.Tempo);
                this.ValidarPotencia(Configuracao.Potencia);
            }
            catch (Exception excecao)
            {
                throw new Exception("Microondas: " + excecao.Message);
            }
        }

        public Programa ExecutarInicioRapido(string nomePrograma, List<Programa> programas)
        {
            var programa = new Programa();
            try
            {
                programa = this.validarPrograma(nomePrograma, programas);
            }
            catch (Exception excecao)
            {
                throw new Exception("Microondas: " + excecao.Message);
            }
            return programa;
        }
    }
}