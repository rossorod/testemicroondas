﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TelaPrincipal.aspx.cs" Inherits="microondas.Interface.TelaPrincipal" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Label ID="labComida" runat="server" Text="Comida"></asp:Label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:TextBox ID="txtComida" runat="server"></asp:TextBox>
            <br />
            <asp:Label ID="labPotencia" runat="server" Text="Potência"></asp:Label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:TextBox ID="txtPotencia" runat="server"></asp:TextBox>
            <br />
            <asp:Label ID="labTempo" runat="server" Text="Tempo em seg."></asp:Label>
&nbsp;&nbsp;
            <asp:TextBox ID="txtTempo" runat="server"></asp:TextBox>
            <br />
            <br />
            <asp:Label ID="lblTimer" runat="server" Font-Bold="True" Font-Size="Larger" Text="00:00"></asp:Label>
            <br />
            <asp:Label ID="lblComidaAquecida" runat="server" Text="Comida aquecida: "></asp:Label>
            <asp:Label ID="lblComidaEsquentada" runat="server"></asp:Label>
            <br />
            <br />
            <asp:Button ID="btnAquecer" runat="server" OnClick="btnAquecer_Click" Text="Aquecer" />
            <asp:Button ID="btnInicioRapido" runat="server" OnClick="btnInicioRapido_Click" Text="Início Rapido" />
            <asp:Button ID="btnNovoPrograma" runat="server" OnClick="btnNovoPrograma_Click" Text="Novo Programa" />
            <br />
            <br />
            <asp:Label ID="lblNovoNome" runat="server" Text="Nome"></asp:Label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:TextBox ID="txtNovoNome" runat="server" Width="120px"></asp:TextBox>
            <br />
            <asp:Label ID="lblNovaDescricao" runat="server" Text="Descrição"></asp:Label>
&nbsp;&nbsp;&nbsp;
            <asp:TextBox ID="txtNovaDescricao" runat="server"></asp:TextBox>
&nbsp;<br />
            <asp:Label ID="lblNovaPotência" runat="server" Text="Potência"></asp:Label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:TextBox ID="txtNovaPotencia" runat="server"></asp:TextBox>
            <br />
            <asp:Label ID="lblNovoTempo" runat="server" Text="Tempo"></asp:Label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:TextBox ID="txtNovoTempo" runat="server"></asp:TextBox>
            <br />
            <asp:Label ID="lblNovoIndicador" runat="server" Text="Indicador"></asp:Label>
&nbsp;&nbsp;&nbsp;
            <asp:TextBox ID="txtNovoIndicador" runat="server"></asp:TextBox>
            <br />
            <br />
            <asp:Button ID="btnSalvar" runat="server" OnClick="btnSalvar_Click" Text="Salvar" />
            <asp:Button ID="btnCancelar" runat="server" OnClick="btnCancelar_Click" Text="Cancelar" />
            <br />
        </div>
        <asp:Timer ID="Timer1" runat="server" Enabled="False" Interval="1000" OnTick="Timer1_Tick">
        </asp:Timer>
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
    </form>
</body>
</html>
