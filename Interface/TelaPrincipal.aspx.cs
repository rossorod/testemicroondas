﻿namespace microondas.Interface
{
    using microondas.Negocio.Dominio;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows.Forms;

    public partial class TelaPrincipal : System.Web.UI.Page
    {
        private Configuracao configuracoes = new Configuracao();
        private int tempo = 0;
        private int minutos = 0;
        private int segundos = 0;
        private string comida;
        private List<Programa> _Programas = Programa.IniciarProgramas();
        protected void Page_Load(object sender, EventArgs e)
        {
            lblComidaEsquentada.Visible = false;
            lblComidaAquecida.Visible = false;
            lblNovaDescricao.Visible = false;
            lblNovaPotência.Visible = false;
            lblNovoIndicador.Visible = false;
            lblNovoNome.Visible = false;
            lblNovoTempo.Visible = false;
            txtNovoIndicador.Visible = false;
            txtNovoNome.Visible = false;
            txtNovoTempo.Visible = false;
            txtNovaDescricao.Visible = false;
            txtNovaPotencia.Visible = false;
            btnSalvar.Visible = false;
            btnCancelar.Visible = false;
        }

        protected void btnAquecer_Click(object sender, EventArgs e)
        {
            this.comida = txtComida.Text;
            tempo = Convert.ToInt16(txtTempo.Text);
            if (tempo >= 60)
            {
                minutos = tempo / 60;
                segundos = tempo % 60;
            }
            else
            {
                minutos = 0;
                segundos = tempo;
            }
            this.configuracoes = new Configuracao();
            this.configuracoes.Potencia = Convert.ToInt32(txtPotencia.Text);
            this.configuracoes.Tempo = new DateTime(0001, 1, 1, 0, minutos, segundos);
            Microondas microondas = new Microondas();
            microondas.Executar(configuracoes);
            lblTimer.Text = "0" + minutos + ":" + segundos;
            Timer1.Enabled = true;
        }

        protected void btnInicioRapido_Click(object sender, EventArgs e)
        {
            Microondas microondas = new Microondas();
            var programa = microondas.ExecutarInicioRapido(txtComida.Text, this._Programas);
            this.ConverterTempo(programa.Tempo);
            lblTimer.Text = "0" + this.minutos + ":" + this.segundos;
            txtTempo.Text = tempo.ToString();
            txtPotencia.Text = programa.Potencia.ToString();
            Timer1.Enabled = true;
        }

        protected void Timer1_Tick(object sender, EventArgs e)
        {
            this.comida = txtComida.Text;
            this.ConverterTempo(Convert.ToInt16(txtTempo.Text));
            for (int i = 0; i < ((Convert.ToInt16(txtPotencia.Text)) * this.segundos); i++)
            {
                this.comida += ".";
            } 
            segundos--;
            if (this.minutos > 0)
            {
                if (this.segundos < 0)
                {
                    this.segundos = 59;
                    this.minutos--;
                }
            }
            //lblTimer.Text = "0" + minutos + ":" + segundos;
            lblTimer.Text = "00:00";
            //if ( minutos < 1 && segundos < 1)
            //{
                Timer1.Enabled = false;
                lblComidaEsquentada.Text = this.comida;
                lblComidaAquecida.Visible = true;
                lblComidaEsquentada.Visible = true;
            //}
            //if (!Timer1.Enabled)
            //{
            //    MessageBoxButtons buttons = MessageBoxButtons.OK;
            //    MessageBox.Show("Sua comida está aquecida", "Pronto", buttons);
            //}
        }

        private void ConverterTempo(int tempo)
        {
            if (tempo >= 60)
            {
                this.minutos = tempo / 60;
                this.segundos = tempo % 60;
            }
            else
            {
                this.minutos = 0;
                this.segundos = tempo;
            }
        }

        protected void btnNovoPrograma_Click(object sender, EventArgs e)
        {
            lblNovaDescricao.Visible = true;
            lblNovaPotência.Visible = true;
            lblNovoIndicador.Visible = true;
            lblNovoNome.Visible = true;
            lblNovoTempo.Visible = true;
            txtNovoIndicador.Visible = true;
            txtNovoNome.Visible = true;
            txtNovoTempo.Visible = true;
            txtNovaDescricao.Visible = true;
            txtNovaPotencia.Visible = true;
            btnSalvar.Visible = true;
            btnCancelar.Visible = true;
        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            lblNovaDescricao.Visible = false;
            lblNovaPotência.Visible = false;
            lblNovoIndicador.Visible = false;
            lblNovoNome.Visible = false;
            lblNovoTempo.Visible = false;
            txtNovoIndicador.Visible = false;
            txtNovoNome.Visible = false;
            txtNovoTempo.Visible = false;
            txtNovaDescricao.Visible = false;
            txtNovaPotencia.Visible = false;
            btnSalvar.Visible = false;
            btnCancelar.Visible = false;
        }

        protected void btnSalvar_Click(object sender, EventArgs e)
        {
            var novoPrograma = new Programa(txtNovoNome.Text, txtNovaDescricao.Text, Convert.ToInt16(txtNovaPotencia.Text), Convert.ToInt16(txtNovoTempo.Text), txtNovoIndicador.Text.ToCharArray().FirstOrDefault());
            this._Programas.Add(novoPrograma);
            lblNovaDescricao.Visible = false;
            lblNovaPotência.Visible = false;
            lblNovoIndicador.Visible = false;
            lblNovoNome.Visible = false;
            lblNovoTempo.Visible = false;
            txtNovoIndicador.Visible = false;
            txtNovoNome.Visible = false;
            txtNovoTempo.Visible = false;
            txtNovaDescricao.Visible = false;
            txtNovaPotencia.Visible = false;
            btnSalvar.Visible = false;
            btnCancelar.Visible = false;
        }
    }
}