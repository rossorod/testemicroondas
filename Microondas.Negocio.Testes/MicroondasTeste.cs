﻿using System;
using System.Collections.Generic;
using microondas.Negocio.Dominio;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Negocio.Testes
{
    [TestClass]
    public class MicroondasTeste
    {
        [TestMethod]
        public void CriarUmaNovaConfiguracaoSemSetarAPotenciaParaVerificarPotenciaPadrao()
        {
            Configuracao config = new Configuracao();
            Assert.AreEqual(config.Potencia, 10);
        }

        [TestMethod]
        public void EsperoSucessoNoMetodoExecutar()
        {
            Configuracao config = new Configuracao();
            config.Tempo = new DateTime(0001, 1, 1, 0, 0, 30);
            Microondas micro = new Microondas();
            try
            {
                micro.Executar(config);
            }
            catch (Exception excecao)
            {
                throw new Exception(excecao.Message);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void EsperoFalhaNoMetodoExecutarComPotenciaInvalida()
        {
            Configuracao config = new Configuracao();
            config.Potencia = 11;
            config.Tempo = new DateTime(0001, 1, 1, 0, 0, 30);
            Microondas micro = new Microondas();
            micro.Executar(config);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void EsperoFalhaNoMetodoExecutarComTempoZerado()
        {
            Configuracao config = new Configuracao();
            config.Tempo = new DateTime(0001, 1, 1, 0, 0, 0);
            Microondas micro = new Microondas();
            micro.Executar(config);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void EsperoFalhaNoMetodoExecutarComTempoAcimaDoLimite()
        {
            Configuracao config = new Configuracao();
            config.Tempo = new DateTime(0001, 1, 1, 0, 2, 1);
            Microondas micro = new Microondas();
            micro.Executar(config);
        }

        [TestMethod]
        public void EsperoSucessoNoMetodoExecutarInicioRapido()
        {
            List<Programa> programas = Programa.IniciarProgramas();
            Microondas micro = new Microondas();
            micro.ExecutarInicioRapido("Frango", programas);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void EsperoFalhaAoTentarExecutarInicioRapidoComProgramaInexistente()
        {
            List<Programa> programas = Programa.IniciarProgramas();
            Microondas micro = new Microondas();
            micro.ExecutarInicioRapido("Carne", programas);
        }
    }
}
